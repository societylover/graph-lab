TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        Graf.cpp \
        adj_mtrx.cpp \
        inc_mtrx.cpp \
        list_adj_vert.cpp \
        main.cpp

HEADERS += \
    Graf.h \
    _using.h \
    adj_mtrx.h \
    inc_mtrx.h \
    list_adj_vert.h
