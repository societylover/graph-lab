#include "inc_mtrx.h"

inc_mtrx::inc_mtrx(const std::shared_ptr<matrix_adj_inc> &Transform1) noexcept : pToAdjMtrx(Transform1)
{
    generateIncMtrx();
}

const std::shared_ptr<matrix_adj_inc> inc_mtrx::getIncMtrxP() const noexcept
{
    return pToIncMtrx;
}

void inc_mtrx::generateIncMtrx() noexcept
{
    auto ribs_count = [this](){ // - Подсчет ребер : в матрице по заданию их количество соединенных вершин / 2
        auto result = 0;
        for (const auto &i : *pToAdjMtrx)
            for (const auto &j : i) if (j == 1) result++;
        return result / 2;
    };

    pToIncMtrx = std::make_shared<matrix_adj_inc>(matrix_adj_inc(pToAdjMtrx->size(), std::vector<digit>(ribs_count(), 0))); // - Указатель на матрицу инцидентности
    fill_ribs_vector(); // - Получаем ребра - соединенные вершины
    fill_inc_Mtrx();    // - Заполняю матрицу инцидентности
}

void inc_mtrx::fill_ribs_vector() noexcept // - Получаем вершины, между которыми есть ребра
{
    auto unique_clear = [this](const std::pair<digit, digit> &tmp){ for (const auto &i : start_end_ribs) // - Проверка на уникальность 01 == 10 (чтобы не добавлять 10, если есть 01)
                                                                    if ((i.first == tmp.first  && i.second == tmp.second) ||
                                                                        (i.second == tmp.first && i.first == tmp.second)) return false;
                                                                        return true;};
    for (digit i = 0; i < pToAdjMtrx->size(); i++)
        for (digit j = 0; j < pToAdjMtrx->size(); j++)
            if (pToAdjMtrx->operator[](i)[j] == 1 && unique_clear({i,j}))
                            start_end_ribs.push_back(std::make_pair(i,j));
}

void inc_mtrx::fill_inc_Mtrx() noexcept // - Заполняем матрицу инцидентности
{
    auto isRib = [](const std::pair<digit, digit> &rib,const digit &val){ // - Проверка, "есть ли вершина в ребре" (0 есть в 0-1 и 0-2, но нет в 1-2)
                    return (rib.first == val || val == rib.second);};

    for (digit i = 0; i < pToIncMtrx->size(); i++)
        for (digit j = 0; j < start_end_ribs.size(); j++)
           if (isRib(start_end_ribs[j], i)) pToIncMtrx->operator[](i)[j] = 1; // - Если вершина образует ребро с другой
}

// - Вернем список ребер (нужно для формирования списка смежных вершин и корректной записи в файл - без доп. расчетов)
const std::shared_ptr<vector<std::pair<digit, digit>>> inc_mtrx::getSignaturesMtrx() const noexcept
{
    return std::make_shared<std::vector<std::pair<digit,digit>>>(start_end_ribs);
}
