#include "Graf.h"

Graf::Graf() noexcept : pVertMtrx(std::make_shared<list_adj_vert>(pIncMtrx->getIncMtrxP(), pIncMtrx->getSignaturesMtrx())),
                        pIncMtrx(std::make_shared<inc_mtrx>(AdjMtrx.getAdjMtrxP())),
                        AdjMtrx()
{}

// - Из вектора списков в вектор векторов
const std::shared_ptr<matrix_adj_inc> Graf::transform_from_veclist_to_vecvec() const noexcept
{
    std::shared_ptr<matrix_adj_inc> result = std::make_shared<matrix_adj_inc>(matrix_adj_inc());
    for (auto i : *pVertMtrx->getVertMtrxP()) result->push_back(vector<digit>(i.begin(), i.end()));
    return result;
}

// - Запись матриц в файлы
void Graf::writeRepresentation(const GrafType &fNames = adjacency_matrix) const noexcept
{
    std::shared_ptr<matrix_adj_inc> pToData;
    auto newFName = [&fNames, &pToData, this](){ switch(fNames) {
        case adjacency_matrix: pToData = AdjMtrx.getAdjMtrxP();   return Graph;
        case incidence_matrix: pToData = pIncMtrx->getIncMtrxP(); return Graph1;
        default:  pToData = transform_from_veclist_to_vecvec();   return Graph2;
        }};

    std::ofstream file(static_cast<std::string>(newFName()));
    if (file.is_open())
        for (const auto &i : *pToData)
        {
            for (const auto &j : i) file << static_cast<int>(j) << '\t';
            file << '\n';
        }
    file.close();
}

const std::shared_ptr<matrix_adj_inc> Graf::getAdjMtrxP() const noexcept
{
    return AdjMtrx.getAdjMtrxP();
}


// - Расчет степеней вершин в зависимости от fNames - не рассматриваются иные величины
const std::shared_ptr<vector<digit>> Graf::calculateDegree(const GrafType &fNames) const noexcept
{
     std::shared_ptr<vector<digit>> result = std::make_shared<vector<digit>>();
     std::shared_ptr<matrix_adj_inc> pToData;

     switch (fNames) {
        case adjacency_matrix:  pToData = AdjMtrx.getAdjMtrxP();
        break;
        case incidence_matrix:  pToData = pIncMtrx->getIncMtrxP();
        break;
        default: pToData = transform_from_veclist_to_vecvec(); }

    if (fNames != adjacent_vertices){
        digit degree = 0;
        for (const auto &i : *pToData) {
            for (const auto &j : i) if (j == 1) degree++;
                result->push_back(static_cast<int>(degree));
                degree = 0; }}
    else for (const auto &i : *pToData) result->push_back(i.size());
    if (fNames == adjacency_matrix) writeAdjInFile(*result);
    return result;
}

// - Запись в файл матрицы смежности
inline void Graf::writeAdjInFile(const std::vector<digit> &vecToData) const noexcept
{
    std::ofstream file(static_cast<std::string>(Degree));
    if (file.is_open()) for (const digit &i : vecToData) file << static_cast<int>(i) << '\n';
    file.close();
}


