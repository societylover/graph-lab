#ifndef INC_MTRX_H
#define INC_MTRX_H

#include "_using.h"
#include "adj_mtrx.h"

// - Второе представление - матрица инцидентности
class inc_mtrx
{
public:
    inc_mtrx() = delete;
    inc_mtrx(const std::shared_ptr<matrix_adj_inc> &Transform1) noexcept;
    const std::shared_ptr<matrix_adj_inc> getIncMtrxP() const noexcept;
    const std::shared_ptr<vector<std::pair<digit, digit>>> getSignaturesMtrx() const noexcept; // - Для возвращения ребер (0-1 1-2 2-3 и т.д.)
private:
    // - Указатели на матрицу смежности и новую матрицу инцидентности
    const std::shared_ptr<matrix_adj_inc> pToAdjMtrx;
    std::shared_ptr<matrix_adj_inc> pToIncMtrx;
    // - Для получения ребер
    vector<std::pair<digit, digit>> start_end_ribs;
    // - Вспомогательные методы
    void generateIncMtrx() noexcept;
    void fill_ribs_vector() noexcept;
    void fill_inc_Mtrx() noexcept;
};

#endif // INC_MTRX_H
