#include <iostream>

#include "Graf.h"

using namespace std;

// - ������ �� �����
vector<int> readDegreeFromFile() noexcept
{
        std::ifstream file("Degree.txt");
        vector<int> degree_result;
        int tmp;
        while (file >> tmp) { degree_result.push_back(tmp);}
        return degree_result;
}

// - �������� ������� � �������� ������ � �����
bool CorrectnessOfTheorem (const std::shared_ptr<matrix_adj_inc> &val)
{
    auto degres_in_file = readDegreeFromFile();
    auto degree_sum = std::accumulate(begin(degres_in_file), end(degres_in_file), 0);

    int all_ribs = 0;

    for (digit i = 0; i < val->size(); i++)
        for (digit j = i + 1; j < val->size(); j++) if (val->operator[](i)[j] == 1) all_ribs++;

    return (degree_sum / 2 == all_ribs);
}


// - ��������� ����������� �������� ������ - ������ ��������� �� ���������� �� Degree
void isCalculatedDegreesEqual(const std::shared_ptr<vector<digit>> &val) noexcept
{
    auto convert_to_int = [&val](){ std::vector<int> res(val->begin(), val->end()); return res; };  // - ������� �� ������� uint8_t � ������ int
    static auto read_from_degree = readDegreeFromFile();
    if (read_from_degree == convert_to_int()) std::cout << "���������� ������� �����!"   << std::endl;
    else                                      std::cout << "���������� ������� �������!" << std::endl;
}

int main()
{
    cout << "������������ ������ � 1, ������� 10. \"�������������� ������������� �����\". \n�������� �.�. �����-31" << endl;
    cout << "������������ ��������� ������� ����������������� ���� ��� ������ � ������� ���� � ����� �� ������ �������������.\n"
            "����������� �������������� ����� �� ������ ������������� � ������. ����������� ������� ������." <<endl;
    cout << "������� ��������� -> ������� ������������� -> C����� ������� ������" << endl;

    Graf a;
    cout <<
    "1.	������������� ���������� ������ � �����\n"
    "2.	� ������������ � ��������� ������������� ��������� ������\n"
    "3.	������� ��������������� ������������� � ��������� ���� Graph\n";
    a.writeRepresentation(adjacency_matrix);
    cout <<
    "4.	��������� ������� ������ � ������� � ��������� ���� Degree\n";
    a.calculateDegree(adjacency_matrix);
    cout <<
    "5.	��������� �������������� 1\n"
    "6.	������� ��������������� ������������� � ��������� ���� Graph 1\n";
    a.writeRepresentation(incidence_matrix);
    cout <<
    "7.	��������� ������� ������ � �������� � ������ Degree\n";
    isCalculatedDegreesEqual(a.calculateDegree(incidence_matrix));
    cout <<
    "8.	��������� �������������� 2\n"
    "9.	������� ��������������� ������������� � ��������� ���� Graph 2\n";
    a.writeRepresentation(adjacent_vertices);
    cout <<
    "10.��������� ������� ������ � �������� � ������ Degree" << endl;
    isCalculatedDegreesEqual(a.calculateDegree(adjacent_vertices));
    cout << "\n������� � �������� ������ � ������ ";
    cout <<((CorrectnessOfTheorem(a.getAdjMtrxP()) == true) ? "�����!" : "�� �����!") <<"\n\n";
    return 0;
}
