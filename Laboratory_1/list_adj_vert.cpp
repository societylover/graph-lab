#include "list_adj_vert.h"

list_adj_vert::list_adj_vert(const std::shared_ptr<matrix_adj_inc> &inc_mtrx,
                             const std::shared_ptr<vector<std::pair<digit, digit>>> &signs)
                             noexcept : inc_mtrx(inc_mtrx), inc_signature(signs)
{
    generateVertMtrx();
}

const std::shared_ptr<matrix_adj_vert> list_adj_vert::getVertMtrxP() const noexcept
{
    return vert_mtrx;
}

// - Генерация списка смежных вершин
void list_adj_vert::generateVertMtrx() noexcept
{
    vert_mtrx = std::make_shared<matrix_adj_vert>(inc_mtrx->size()); // - Создадим вектор без элементов в списке

    auto another_vertex = [](const digit& vert, const std::pair<digit, digit> two_verts){  // - Ищем вершину отличную от просматриваемой строки (0-1 ребро, смотрим 0 -> искомая вершина 1)
                             return vert == two_verts.first ? two_verts.second : two_verts.first; };

    for (digit i = 0; i < vert_mtrx->size(); i++)
        for (digit j = 0; j < inc_mtrx->operator[](i).size(); j++)
             if (inc_mtrx->operator[](i)[j] == 1) vert_mtrx->operator[](i).push_back(another_vertex(i, inc_signature->operator[](j)));
}
