#ifndef _USING_H
#define _USING_H

#include <cstdint>
#include <stdexcept>
#include <random>
#include <memory>
#include <chrono>
#include <ctime>
#include <algorithm>
#include <string_view>

#include <vector>
#include <list>

#include <fstream>

using std::vector;
using std::list;

using digit = uint8_t;

using matrix_adj_inc = vector<vector<digit>>; // - Для матриц смежности и инциндентности
using matrix_adj_vert = vector<list<digit>>;  // - Для матриц смежных вершин

#endif // _USING_H
