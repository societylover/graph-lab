#ifndef GRAF_H
#define GRAF_H

#include "adj_mtrx.h"
#include "inc_mtrx.h"
#include "list_adj_vert.h"

enum GrafType
{
    adjacency_matrix,
    incidence_matrix,
    adjacent_vertices
};

class Graf
{
public:
     Graf() noexcept;
     void writeRepresentation(const GrafType &fNames) const noexcept;
     // - Подсчет степеней вершин (для матрицы смежности еще и запись в файл)
     const std::shared_ptr<vector<digit>> calculateDegree(const GrafType &fNames) const noexcept;
     const std::shared_ptr<matrix_adj_inc> getAdjMtrxP() const noexcept; // - Для проверки теоремы
private:
     // - Матрица и указатели на матрицы
     adj_mtrx AdjMtrx;
     std::shared_ptr<inc_mtrx> pIncMtrx;
     std::shared_ptr<list_adj_vert> pVertMtrx;
     // - Запись матрицы смежности в файл
     inline void writeAdjInFile(const std::vector<digit> &vecToData) const noexcept;
     // - Вспомогательный метод перевода из вектора списков в вектор векторов
     const std::shared_ptr<matrix_adj_inc> transform_from_veclist_to_vecvec() const noexcept;
     // - Имена для записи представлений
     constexpr static std::string_view Degree = "Degree.txt";
     constexpr static std::string_view Graph = "Graph.txt";
     constexpr static std::string_view Graph1 = "Graph1.txt";
     constexpr static std::string_view Graph2 = "Graph2.txt";
};

#endif // GRAF_H
