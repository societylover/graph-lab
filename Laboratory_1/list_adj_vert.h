#ifndef LIST_ADJ_VERT_H
#define LIST_ADJ_VERT_H

#include "_using.h"

class list_adj_vert // - Последнее преобразование - список смежных вершин
{
public:
    list_adj_vert() = delete;
    list_adj_vert(const std::shared_ptr<matrix_adj_inc> &inc_mtrx,
                  const std::shared_ptr<vector<std::pair<digit, digit>>> &signs) noexcept;
    const std::shared_ptr<matrix_adj_vert> getVertMtrxP() const noexcept;
private:
    // - Генерация значений
    void generateVertMtrx() noexcept;
    std::shared_ptr<matrix_adj_inc> inc_mtrx;
    // - Ребра полученные в матрице инцидентности (для отсутствия пересчета)
    std::shared_ptr<vector<std::pair<digit, digit>>> inc_signature;
    std::shared_ptr<matrix_adj_vert> vert_mtrx;
};

#endif // LIST_ADJ_VERT_H
