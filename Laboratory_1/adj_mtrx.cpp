#include "adj_mtrx.h"

inline digit generate_size(std::mt19937 &rnd) noexcept
{
    static std::uniform_int_distribution<> distrib(lower_gen, higher_gen);
    return distrib(rnd);
}

adj_mtrx::adj_mtrx() : pMtrx(std::make_shared<matrix_adj_inc>(matrix_adj_inc(generate_size(rnd)))),
                       rnd((std::chrono::system_clock::now().time_since_epoch().count()))
{
    // - Инициализация строк матрицы
    for (auto &i : *pMtrx) i = vector<digit>(pMtrx->size(), 0);
    generate_value();
}

void adj_mtrx::generate_value() noexcept
{
    static std::uniform_int_distribution<> distrib(0, 1);
    // - Генерация ребер
    for (unsigned i = 0; i < pMtrx->size(); i++) {
        for (unsigned j = i + 1; j < pMtrx->size(); j++)
            pMtrx->operator[](i)[j] = pMtrx->operator[](j)[i] = distrib(rnd);
        pMtrx->operator[](i)[i] = pMtrx->operator[](i)[i] = 0; // - Уберем петли
    }
    // - Добавим ребер, где нет "связности"
    for (unsigned i = 0; i < pMtrx->size(); i++)
    {
        if (!std::any_of( pMtrx->operator[](i).begin(),
                          pMtrx->operator[](i).end(),
                          [](const digit &i){ return i == 1; } ))
        { static std::uniform_int_distribution<> distrib(0, pMtrx->size());
          unsigned rnd_ind = distrib(rnd);
          while (rnd_ind == i) rnd_ind = distrib(rnd);
          pMtrx->operator[](i)[rnd_ind] = pMtrx->operator[](rnd_ind)[i] = 1;
        }
    }
}

const std::shared_ptr<matrix_adj_inc> adj_mtrx::getAdjMtrxP() const noexcept
{
    return pMtrx;
}
