﻿#ifndef ADJ_MTRX_H
#define ADJ_MTRX_H

#include "_using.h"

// - Границы генерации количества вершин
static constexpr digit lower_gen = 10;
static constexpr digit higher_gen = 15;

// - Начальное представление - матрица смежности
class adj_mtrx
{
public:
    adj_mtrx();
    // - Значений сгенерированной матрицы смежности
    const std::shared_ptr<matrix_adj_inc> getAdjMtrxP() const noexcept;
private:
    // - Генерация значений, генератор и указатель на значения
    void generate_value() noexcept;
    std::mt19937 rnd;
    std::shared_ptr<matrix_adj_inc> pMtrx;
};

#endif // ADJ_MTRX_H
