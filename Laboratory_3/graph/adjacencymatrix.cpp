﻿#include "adjacencymatrix.h"


// - Генерация значений (как размера, так и связности вершин)
AdjacencyMatrix::value AdjacencyMatrix::generateNumeric(const AdjacencyMatrix::value lower_bound,
                                                        const AdjacencyMatrix::value higher_bound) const noexcept
{
    std::mt19937 rnd(std::chrono::system_clock::now().time_since_epoch().count());
    std::uniform_int_distribution<> generator (lower_bound, higher_bound);
    return generator(rnd);
}



AdjacencyMatrix::AdjacencyMatrix() noexcept :
     Matrix(std::make_shared<adj_matrix>(generateNumeric(low_bound,high_bound))) // - Генерируем размерности матрицы
{
     auto maxRibsCount = Matrix->size()*(Matrix->size()- 1) / 2;
     auto ribs_count = generateNumeric(std::ceil(maxRibsCount / 2.), maxRibsCount); // - Количество ребер графа

     for (auto &i : *Matrix) i = row(Matrix->size(), 0);

     // - Получим номер вершины для коннекта
     auto connectVert = [&](const auto &i){
        auto randomVert = generateNumeric(0, Matrix->size()-1);
        std::set<value> variants;
        // - Пока не перебрали все варианты и нет петли и кратного ребра
        while (variants.size() != Matrix->size() &&
               (i == randomVert || (Matrix->operator[](i)[randomVert] == 1 || Matrix->operator[](randomVert)[i] == 1)))
        {
          randomVert = generateNumeric(0, Matrix->size()-1);
          variants.insert(randomVert);
        }
        // - Если невозможно подобрать, то -1, иначе сгенерироваенное значение
        return variants.size() == Matrix->size() ? -1 : randomVert;
     };

     while (ribs_count) // - Пока все ребра не были использованы
     {
          for (std::vector<int>::size_type i = 0; ribs_count && i < Matrix->size(); i++)
          {
              if (generateNumeric(0,3) > 2)
              {
                  auto j_index = connectVert(i);
                  if (j_index >= 0)
                  {
                  Matrix->operator[](i)[j_index] = 1;
                  ribs_count--;
                  }
              }
          }
     }

     for (auto i : *Matrix)
     {
         for (auto j : i) std::cout << (int)j << ' ';
         std::cout << std::endl;
     }
     std::cout << std::endl;
     BftReachabilityCounterReachabilityMatrix();
}

bool AdjacencyMatrix::writeRepresentation(const std::string_view &name, const pToMatrix Matrix) const noexcept
{
    return writeInFile(name, Matrix);
}

Graph::pToMatrix AdjacencyMatrix::getPAdjacencyMatrix() const noexcept
{
    return Matrix;
}

// - Обход графа в ширину без рекурсии
Graph::pToMatrix AdjacencyMatrix::getPReachabilityMatrix() const noexcept // - Получить матрицу достижимости обходом в ширину
{
    return ReachabilityMatrix;
}

// - Построение матрицы достижимости и контрдостижимости обходом в ширину
void AdjacencyMatrix::BftReachabilityCounterReachabilityMatrix() noexcept
{
    ReachabilityMatrix = std::make_shared<adj_matrix>(Matrix->size());
    CounterReachableMatrix = std::make_shared<adj_matrix>(Matrix->size());

    for (auto &i : *ReachabilityMatrix) i = row(Matrix->size(), 0);
    for (auto &i : *CounterReachableMatrix) i = row(Matrix->size(), 0);

    auto adj_list = [&](const value &v) // - Список ребер по матрице смежности
    {
        row result;
        for (size_t i = 0; i < Matrix->size(); i++)
         if (Matrix->operator[](v)[i]) result.push_back(i);
        return result;
    };

    auto clear_queue = [](queue<value> &a)
    {
        while (!a.empty()) a.pop();
    };

    for (std::vector<value>::size_type i = 0; i < Matrix->size(); i++)
    {
        for (std::vector<value>::size_type j = 0; j < Matrix->size(); j++)
        {
        if (j != i)
        { // - Не регистрировать попадение самого в себя
        queue<value> element_queue; // - Очередь из элементов
        element_queue.push(i); // - Сразу посещаем первый элемент
        while (!element_queue.empty())
        {
            auto adj_list_result = adj_list(element_queue.front());
            element_queue.pop();
            if (std::any_of(begin(adj_list_result), end(adj_list_result), [&](const value &v) {return v == j;}))
            {
                ReachabilityMatrix->operator[](i)[j] = 1;
                CounterReachableMatrix->operator[](j)[i] = 1;
                clear_queue(element_queue);
            } else
            {
                for (auto z : adj_list_result) {
                    element_queue.push(z);
                }
            }
        }
        } else
        {
            ReachabilityMatrix->operator[](i)[j] = 1; // - Из вершины А я попаду в вершину А (я в ней)
            CounterReachableMatrix->operator[](j)[i] = 1;
        }

        }
     }

    for (auto i : *ReachabilityMatrix){
        for (auto j : i) std::cout << (int)j << ' ';
        std::cout << std::endl;
    }
    std::cout << std::endl;
    for (auto i : *CounterReachableMatrix){
        for (auto j : i) std::cout << (int)j << ' ';
        std::cout << std::endl;
    }
}


