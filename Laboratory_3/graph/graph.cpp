﻿#include "graph.h"

bool Graph::writeInFile(const std::string_view &name, const Graph::pToMatrix Matrix) const noexcept
{
    std::ofstream file(name.data());
    if (file.is_open())
    {
        std::for_each(begin(*Matrix), end(*Matrix), [&](row &a)
            { for (const auto &i : a) file << static_cast<int>(i) << ' '; file << '\n'; }); // - Перепишем значения в файл
        file.close();
        return true;
    }
    return false; // - Запись не удалась!
}

bool Graph::isVisited(const value &v, const row &visited) const noexcept
{
     return std::any_of(begin(visited), end(visited), [v](const value &i) { return v == i; });
}
