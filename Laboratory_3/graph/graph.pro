TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        adjacencymatrix.cpp \
        graph.cpp \
        main.cpp

HEADERS += \
    adjacencymatrix.h \
    graph.h
