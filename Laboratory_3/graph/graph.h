﻿#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include <chrono>
#include <random>
#include <memory>
#include <algorithm>

#include <string_view>
#include <fstream>
#include <set>
#include <queue>

#include <iostream>

using std::queue;

class Graph
{
public:
    // - Основные переопределения
    using value = uint8_t;
    using row = std::vector<value>;
    using adj_matrix = std::vector<row>;
    using pToMatrix = std::shared_ptr<adj_matrix>;

    virtual bool writeRepresentation(const std::string_view &name, const pToMatrix Matrix) const noexcept = 0;

 protected:
    bool isVisited (const value &v, const row &visited) const noexcept;
    bool writeInFile(const std::string_view &, const pToMatrix Matrix) const noexcept; // - У каждого свой указатель
};

#endif // GRAPH_H
