﻿#ifndef ADJACENCYMATRIX_H
#define ADJACENCYMATRIX_H

#include "graph.h"

class AdjacencyMatrix : public Graph
{
    pToMatrix Matrix;               // - Указатель на матрицу смежности
    pToMatrix ReachabilityMatrix;   // - Указатель на матрицу достижимости
    pToMatrix CounterReachableMatrix; // - Указатель на матрицу контрдостижимости
    // - Для генерации размерности матрицы
    constexpr static AdjacencyMatrix::value low_bound  = 3;
    constexpr static AdjacencyMatrix::value high_bound = 5;
    value generateNumeric(const value lower_bound = low_bound, const AdjacencyMatrix::value higher_bound = high_bound) const noexcept;
    void BftReachabilityCounterReachabilityMatrix() noexcept; // - Построение матрицы достижимости и контрдостижимости обходом в ширину
public:

    AdjacencyMatrix() noexcept;


    pToMatrix getPAdjacencyMatrix() const noexcept;             // - Получить указатель на матрицу смежности
    pToMatrix getPReachabilityMatrix() const noexcept;          // - Получить указатель на матрицу достижимости обходом в ширину
    pToMatrix getPCounterreachableMatrix() const noexcept;      // - Получить указатель на матрицу контрдостижимости обходом в ширину


    bool writeRepresentation(const std::string_view &name, const pToMatrix Matrix) const noexcept override;



};

#endif // ADJACENCYMATRIX_H
