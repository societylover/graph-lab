﻿#include "graph.h"

bool Graph::writeInFileTraverse(const std::string_view &name, const vector<element> &v) const noexcept
{
    std::ofstream files(name.data());

    if (files.is_open())
    {
        for (const auto &i : v) files << static_cast<int>(i) << ' ';
        return true;
    }
    return false;
}

vector<element> Graph::traverse() const noexcept
{
    return do_traverse(0);
}

bool Graph::isVisited(const element &v, const vector<element> &visited) const noexcept
{
     return std::any_of(begin(visited), end(visited), [v](const element &i) { return v == i; });
}

bool Graph::writeInFileRepresentation(const std::string_view &name) const noexcept
{
    return writeRepresentation(name);
}
