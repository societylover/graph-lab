﻿#ifndef GRAPH_H
#define GRAPH_H

#include <string_view>
#include <stack>
#include <iostream>

#include <fstream>

#include <redefinitions.h>

class Graph
{
public:
    vector<element> traverse() const noexcept; // - Обход, который вызовет do_traverse(0)
    bool writeInFileTraverse(const std::string_view &, const vector<element> &) const noexcept; // - Запись обхода в файл
    bool writeInFileRepresentation(const std::string_view &) const noexcept;
protected:
    virtual bool writeRepresentation(const std::string_view &) const noexcept = 0;
    virtual vector<element> do_traverse(const element &v) const noexcept = 0; // - Обход, у каждого предствления свой
    bool isVisited (const element &v, const vector<element> &visited) const noexcept;
};

#endif // GRAPH_H
