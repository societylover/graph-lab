﻿#ifndef LISTOFEDGES_H
#define LISTOFEDGES_H

#include <chrono> // -- Для
#include <ctime>  // -- рандомизации
#include <random> // -- значений
#include <algorithm>

#include "graph.h"

class ListOfEdges : public Graph
{
public:
    ListOfEdges();
    friend class AdjacencyMatrix; // - Для просмотра списка ребер

private:
    bool writeRepresentation(const std::string_view &) const noexcept override;
    vector<element> adjacencyVertList(const element &i) const noexcept;
    vector<element> do_traverse(const element &) const noexcept override;
    std::mt19937 rnd;
    pList_edges pToListOfEdgesPerformance;

    const element count;
};

#endif // LISTOFEDGES_H
