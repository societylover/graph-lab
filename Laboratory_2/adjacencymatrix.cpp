﻿#include "adjacencymatrix.h"

// - Для обхода без рекурсии
#include <queue>

using std::queue;

// - Сразу конструктор заполнит матрицу n * n при помощи 0
AdjacencyMatrix::AdjacencyMatrix(const ListOfEdges &v) noexcept:
    pToValue(std::make_shared<adj_mtrx>(v.count, vector<element>(v.count, 0)))
{   
     // - Получаем список смежных вершин, и по ним строим матрицу смежности
     for (const auto &i : *v.pToListOfEdgesPerformance)
     {
         auto res = v.adjacencyVertList(i.first);
         for (const auto &j : res)
             pToValue->operator[](i.first)[j] =
             pToValue->operator[](j)[i.first] = 1;
     }
}

// - Обход графа в ширину без рекурсии
vector<element> AdjacencyMatrix::do_traverse(const element &v) const noexcept
{
    vector<element> visited(1, v);
    queue<element> element_queue; // - Очередь из элементов
    element_queue.push(v); // - Сразу посещаем первый элемент

    auto adj_list = [&](const element &v) // - Список ребер по матрице смежности
    {
        vector<element> result;
        for (size_t i = 0; i < pToValue->size(); i++)
         if (pToValue->operator[](v)[i]) result.push_back(i);
        return result;
    };

    while (!element_queue.empty())
    {
        auto adj_list_result = adj_list(element_queue.front());
        element_queue.pop();
        for (auto i : adj_list_result)
        {
            if (!isVisited(i, visited)) {
                visited.push_back(i);
                element_queue.push(i);
            }
        }
    }
    return visited;
}

bool AdjacencyMatrix::writeRepresentation(const std::string_view &name) const noexcept
{
    std::ofstream file(name.data());

    if (file.is_open())
    {
        for (const auto &i : *pToValue)
        {
            for (const auto &j : i) file << static_cast<int>(j) << ' ';
            file << '\n';
        }
        return true;
    }
    return false;
}
