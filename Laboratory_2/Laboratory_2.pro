TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt core

SOURCES += \
        adjacencymatrix.cpp \
        graph.cpp \
        listofedges.cpp \
        main.cpp

HEADERS += \
    adjacencymatrix.h \
    graph.h \
    listofedges.h \
    redefinitions.h
