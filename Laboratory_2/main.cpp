﻿#include <iostream>
#include <ctime> // - Для замера времени

#include "listofedges.h"
#include "adjacencymatrix.h"

using namespace std;

int main()
{
    system("chcp 65001"); // - utf8 в консоли

    std::cout << "Кузнецов Е.О., ДИПРб-31. Графы, лабораторная 2. Вариант 5" <<std::endl;
    std::cout << "Тема: алгоритмы обхода графа" << std::endl;
    std::cout << "Генерируется случайный связный неориентированный граф \n"
              "без петель и кратных рёбер в одном из четырёх представлений. \n"
              "Выполняется преобразование графа из одного представления в другое. \n"
              "Выполняется обход графа в глубину и в ширину в каждом из представлений. \n"
              "Определить время выполнения алгоритмов.\n";

    std::cout << "Генерация графа в представлении список ребер"<<std::endl;
    ListOfEdges a;
    std::cout << "Запись графа в виде списка ребер в файл Graph.txt"<<std::endl;
    a.writeInFileRepresentation("Graph.txt");

    std::cout << "Обход графа в виде списка ребер в глубину и замер времени"<<std::endl;
    unsigned int start_time = clock();
    a.writeInFileTraverse("GR1.txt", a.traverse());
    unsigned int end_time = clock();
    std::cout << "Время работы алгоритма и записи в файл  = " << (end_time - start_time) << std::endl;

    std::cout << "Преобразование из списка ребер в список смежности" <<std::endl;
    AdjacencyMatrix b(a);
    std::cout << "Запись графа в виде списка ребер в файл Graph.txt"<<std::endl;
    b.writeInFileRepresentation("Graph1.txt");
    std::cout << "Обход графа в виде списка ребер в ширину и замер времени"<<std::endl;
    start_time = clock();
    b.writeInFileTraverse("GR2.txt", b.traverse());
    end_time = clock();
    std::cout << "Время работы алгоритма и записи в файл  = " <<(end_time - start_time) << std::endl;
    return 0;
}
