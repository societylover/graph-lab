﻿#ifndef REDEFINITIONS_H
#define REDEFINITIONS_H

#include <vector>
#include <cstdint>
#include <memory>
#include <algorithm>

using std::shared_ptr;
using std::vector;
using std::pair;

using element = uint8_t;

using value = std::pair<element, element>;
// - Переопределение первого представления - списка ребер - вектор пар вершин
using list_edges = vector<pair<element, element>>;
// - Переопределение второго представления - матрицы смежности - вектор векторов
using adj_mtrx = vector<vector<element>>;

using pList_edges   = shared_ptr<list_edges>;
using pAdj_mtrx     = shared_ptr<adj_mtrx>;

#endif // REDEFINITIONS_H
