﻿#ifndef ADJACENCYMATRIX_H
#define ADJACENCYMATRIX_H

#include "graph.h"
#include "listofedges.h"

class AdjacencyMatrix : public Graph
{
public:
    AdjacencyMatrix() = delete;
    explicit AdjacencyMatrix(const ListOfEdges &) noexcept;
    ~AdjacencyMatrix() = default;
private:
    bool writeRepresentation(const std::string_view &) const noexcept override;
    vector<element> do_traverse(const element &) const noexcept override; // - Проход в ширину
    pAdj_mtrx pToValue; // - Указатель на матрицу смежности
};

#endif // ADJACENCYMATRIX_H
