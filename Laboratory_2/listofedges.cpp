﻿#include "listofedges.h"

// - Границы генерации количества вершин
static constexpr element lower_bound  = 20;
static constexpr element higher_bound = 30;


// - Генерация количества элементов
inline element generator(std::mt19937 &rnd, const element& lower_bounds = lower_bound,
                                            const element &higher_bounds = higher_bound) noexcept
{
    std::uniform_int_distribution<> distrib(lower_bounds, higher_bounds);
    return distrib(rnd);
}

// - Генерация значений списком ребер
void generate_value(pList_edges pL_e, std::mt19937 &rnd, const element &size) noexcept
{
    // - Случайное значение из списка для связи, где нет значения v (нет петли)
    auto variant_to_connect = [&](const element &v){
        static vector<element> all(size, 0);
        if (all[size-1] == 0) std::iota(begin(all), end(all), 0);  // - Если только создан, заполним его значениями от 0 до size
        v == 0 ? all[size - 1] = size - 1 : all[v - 1] = v - 1;    // - Вернем значение уменьшенному элементу, который мы "испортили" в шаге дальше; Если наш элемент 0, то следует исправить последний элемент
        all[v] = v == 0 ? all[1] : all[v-1]; // - Вместо v подставим значение до него
        return all[generator(rnd, 0, all.size() - 1)]; // - Сгенерируем и вернем значение
    };

    auto isUnique = [&](const value &v) // - Проверка значения на уникальность, т.е. если [0,1] есть, то [1,0] для неориентированного графа уже есть и оно излишне
    {
        for (const auto &i : *pL_e) {
            if ((i.first == v.first && i.second == v.second)
            || (i.first == v.second && i.second == v.first)) return false;
        }
        return true;
    };


    for (auto i = 0, chance = 0; i < size; i++, chance = 0) // - С каким то шансом будем добавлять значение, шанс растет и последнее значение будет связано 100%
    {
        for (auto j = 0; j < size; j++, chance++){
            if (generator(rnd, chance, size) > size / 2) { // - С 50% шансом будет добавляться в начале, а потом будет увеличиваться
                auto value = variant_to_connect(i);
                if (isUnique({i, value}))  pL_e->push_back({i, value});} // - Если значение уникально, запомним его
    }}
}

ListOfEdges::ListOfEdges() : pToListOfEdgesPerformance(std::make_shared<list_edges>()),
                             rnd(std::chrono::system_clock::now().time_since_epoch().count()),
                             count(generator(rnd))
{
    generate_value(pToListOfEdgesPerformance, rnd, count);
}

// - Список смежных вершин, как метод, потому что понадобиться для AdjMatrix
vector<element> ListOfEdges::adjacencyVertList(const element &v) const noexcept
{
    vector<element> result;
    for (const auto &i : *pToListOfEdgesPerformance){
        if (i.first == v)  result.push_back(i.second);
        if (i.second == v) result.push_back(i.first);}
    return result;
}

// - Обход графа в глубину применительно к представлению список ребер - рекурсивно
vector<element> ListOfEdges::do_traverse(const element &v) const noexcept
{
   static vector<element> visited(1, v);

   auto verts = adjacencyVertList(v);

   for (auto i : verts)
   {
       if (!isVisited(i, visited)) {
           visited.push_back(i);
           do_traverse(i); }
   }

   return visited;
}

bool ListOfEdges::writeRepresentation(const std::string_view &name) const noexcept
{
    std::ofstream file(name.data());

    if (file.is_open())
    {
        for (const auto &i : *pToListOfEdgesPerformance) file << static_cast<int>(i.first) << '-' << static_cast<int>(i.second) << '\t';

        return true;
    }
    return false;
}
